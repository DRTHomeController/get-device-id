'use strict';

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://ip-172-31-29-17.ec2.internal:27017/";

exports.getDeviceId = (event, context, callback) => {
    var deviceName = event.deviceId;
    var userId = event.userId;
    
    console.log("Userid: " + userId);

    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var selectedDb = db.db("authInfo");
        
        selectedDb.collection("userDevices").find({"userId":userId,"deviceName":deviceName}).toArray(function(err, result){
            if (err) throw err;
            db.close();
            callback(null, result);
        });
      });
}
